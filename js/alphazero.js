//////////////////////////////////////////////////////////////////////////
// AMMOS.js
//
// Roadmap
// [X] - Make it pretty (shaders: lava)
// [X] - Make it pretty (shaders: glass)
// [X] - Add timer
// [X] - Add HUD (health, armor, game time, weapon, ammo)
// [X] - Verify power ups
// [X] - Crosshair

// [X] - Make it pretty (shaders: water)
// [X] - Sounds
// [ ] - Multiplayer
// [ ] - Add HUD (score, spawn times, ping?)

// [ ] - Refactor (collision function, game loop, hud, update hud on resize)
// [ ] - Doc
// [ ] - start / setting / credits - UI
// [ ] - Make it pretty (add level/ui music)

// [ ] - Add switch
// [ ] - Add moving platforms
// [ ] - Update deathmatch map
// [/] - Make deathmatch map (add maze?/puzzles)

// [ ] - Reflect
// [ ] - Make ctf map
// [ ] - Login Menu
// [ ] - AI

// [ ] - Make it pretty (get new Sounds)
// [X] - Crosshair effect on hover
// [/] - Make it pretty (shaders: clouds)
// [ ] - Make it pretty (shaders: attacks???)
// [ ] - Make it pretty (models)
// [?] - Make it pretty (shaders: lava; on hit effect, vertex shader)
// [ ] - Make it pretty (shaders: glass; on hit effect)
// [ ] - Multiplayer Lobby/Rooms
// [ ] - Multiplayer Reaction board vs chat
// [ ] - Disable camera zoom

// BUGS
// [ ] - Fix mem leaks (Is this due to Ammo.js?)
// [X] - Fix timer
// [X] - Fix glass reflection
// [ ] - Disable wall climbing (use collision contact point)
// [ ] - Restitution doesn't work; should characters be kinematic objects?
// [ ] - Power Ups should be ghost objects
// [X] - Replace cubeCamera with 4 custom cameras and render target
// [ ] - Bullets should start from higher ground
// [ ] - Should bullets be affected by gravity? (kinematic ghost objects?)      
// [ ] - Refactor animation handling into OO design pattern
// [ ] - Character mesh is not aligned when shooting
// [ ] - Rectify collision object for players
// [ ] - z-order players

// WONTFIX
// [ ] - Shadows seem inconsistent on different camera positions
//       Works much better on chrome than FF
// [ ] - Character was exported with wrong colours
// [ ] - body.setCollisionFlags(this.collisionFlags | CUSTOM_MATERIAL_CALLBACK);
//       seems to work ok without it (note: CUSTOM_MATERIAL_CALLBACK == 8)
// [ ] - Render twice? to account for reflective delays
//       Seems fixed after resolution of feedback loop

// OPTS / NTH
// [ ] - Asset load manager and progress
// [ ] - Script/schedule audio
// [ ] - Add water physics
// [ ] - Shrink glass alpha map (and other textures)
// [ ] - Fix text shader for random resolutions
// [ ] - Change player/powerup/bullet lists into dicts
// [ ] - Make lava scale better. (Tamper with noise?)
// [ ] - Boxes should sink in lava
// [ ] - Load maps from JSON
// [ ] - Post prossesing lava effects when nearing lava
// [ ] - Get rid of all warning on FF console (test on Edge/Safari)

"use strict";

var AlphaZero = {timeouts: []};
Ammo().then(function(Ammo) {
  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // Init and global state

  function getLocationParams(q) {
    var hashParams = {};
    var e,
        a = /\+/g,
        r = /([^&;=]+)=?([^&;]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); };
    while (e = r.exec(q)) {
      hashParams[d(e[1])] = d(e[2]);
    }
    return hashParams;
  }

  var querySettings = getLocationParams(window.location.hash.substring(1));
  Object.assign(querySettings, getLocationParams(
      window.location.search.substring(1)));

  function textToTextureIndex(text) {
    var indices = [];
    text = text.toUpperCase();
    for (var ci in text) {
      var code = text.charCodeAt(ci);
      code = (code < 65) ? code : code + 128;
      indices.push(code);
    }
    return indices;
  }

  // Game state

  var uniforms = {time: {value: 1.0}};

  function initRenderer() {
    // create renderer
    var renderer = new THREE.WebGLRenderer();
    AlphaZero.renderer = renderer;
    renderer.shadowMap.enabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMap.type = THREE.PCFShadowMap;
    renderer.setClearColor(0x060606, 1);
    renderer.autoClear = false;
    renderer.setSize(window.innerWidth, window.innerHeight);
    // add canvas to DOM
    document.body.appendChild(renderer.domElement);
  }

  function createHUD() {
    var widgetWidth = window.innerWidth / 20;
    var widgetHeight = window.innerHeight / 20 * window.innerWidth / window.innerHeight;

    var text = "0000"+"XXXX";
    var texture = textureLoader.load('textures/console-font.png');
    texture.magFilter = THREE.NearestFilter;
    texture.minFilter = THREE.NearestFilter;
    var hudUniforms = {
      texSize: {value: new THREE.Vector2(16.0, 16.0)},
      gridSize: {value: new THREE.Vector2(4.0, 2.0)},
      index: {type: "fv1", value: textToTextureIndex(text)},
      uvScale: {value: new THREE.Vector2(1.0, 1.0)},
      fontTexture: {value: texture}
    };
    var geometry = new THREE.PlaneBufferGeometry(2 * widgetWidth, widgetHeight);
    var material = new THREE.ShaderMaterial({
      uniforms: hudUniforms,    
      vertexShader: document.getElementById(
              'hudVertexShader').textContent,
      fragmentShader: document.getElementById(
              'hudFragmentShader').textContent,
      transparent: true
    });
    var mesh = new THREE.Mesh( geometry, material );
    AlphaZero.uiScene.add( mesh );
    mesh.position.set(0, window.innerHeight / 2. - widgetHeight / 2, 1.);
    AlphaZero.hud = mesh;

    var health = "0000"+"0000";
    var healthHudUniforms = {
      texSize: {value: new THREE.Vector2(16.0, 16.0)},
      gridSize: {value: new THREE.Vector2(3.0, 1.0)},
      index: {type: "fv1", value: textToTextureIndex(health)},
      uvScale: {value: new THREE.Vector2(1.0, 1.0)},
      fontTexture: {value: texture}
    };
    var healthGeometry = new THREE.PlaneBufferGeometry(2 * widgetWidth, widgetHeight);
    var healthMaterial = new THREE.ShaderMaterial({
      uniforms: healthHudUniforms,    
      vertexShader: document.getElementById(
              'hudVertexShader').textContent,
      fragmentShader: document.getElementById(
              'hudFragmentShader').textContent,
      transparent: true
    });
    var healthMesh = new THREE.Mesh( healthGeometry, healthMaterial );
    AlphaZero.uiScene.add( healthMesh );
    healthMesh.position.set(- 4.5 * widgetWidth, - window.innerHeight / 2. + widgetHeight / 2, 1.0);
    AlphaZero.hudHealth = healthMesh;

    var armor = "0000"+"0000";
    var armorHudUniforms = {
      texSize: {value: new THREE.Vector2(16.0, 16.0)},
      gridSize: {value: new THREE.Vector2(3.0, 1.0)},
      index: {type: "fv1", value: textToTextureIndex(armor)},
      uvScale: {value: new THREE.Vector2(1.0, 1.0)},
      fontTexture: {value: texture}
    };
    var armorGeometry = new THREE.PlaneBufferGeometry(2 * widgetWidth, widgetHeight);
    var armorMaterial = new THREE.ShaderMaterial({
      uniforms: armorHudUniforms,    
      vertexShader: document.getElementById(
              'hudVertexShader').textContent,
      fragmentShader: document.getElementById(
              'hudFragmentShader').textContent,
      transparent: true
    });
    var armorMesh = new THREE.Mesh(armorGeometry, armorMaterial);
    AlphaZero.uiScene.add(armorMesh);
    armorMesh.position.set(0., - window.innerHeight / 2. + widgetHeight / 2, 1.0);
    AlphaZero.hudArmor = armorMesh;

    var weapon = "0000"+" 13";
    var weaponHudUniforms = {
      texSize: {value: new THREE.Vector2(16.0, 16.0)},
      gridSize: {value: new THREE.Vector2(3.0, 1.0)},
      index: {type: "fv1", value: textToTextureIndex(weapon)},
      uvScale: {value: new THREE.Vector2(1.0, 1.0)},
      fontTexture: {value: texture}
    };
    var weaponGeometry = new THREE.PlaneBufferGeometry(2 * widgetWidth, widgetHeight);
    var weaponMaterial = new THREE.ShaderMaterial({
      uniforms: weaponHudUniforms,    
      vertexShader: document.getElementById(
              'hudVertexShader').textContent,
      fragmentShader: document.getElementById(
              'hudFragmentShader').textContent,
      transparent: true
    });
    var weaponMesh = new THREE.Mesh(weaponGeometry, weaponMaterial);
    AlphaZero.uiScene.add(weaponMesh);
    weaponMesh.position.set(4.5 * widgetWidth, - window.innerHeight / 2. + widgetHeight / 2, 1.0);
    AlphaZero.hudweapon = weaponMesh;

    var healthTexture = textureLoader.load('textures/trak_light2a.jpg');
    texture.magFilter = THREE.NearestFilter;
    texture.minFilter = THREE.LinearMipMapLinearFilter;
    var healthGeometry = new THREE.PlaneBufferGeometry(widgetWidth / 2, widgetHeight / 2);
    var healthMaterial = new THREE.MeshBasicMaterial({map: healthTexture,
          transparent: true,  opacity: .7, depthWrite: false});
    var healthMesh = new THREE.Mesh(healthGeometry, healthMaterial);
    AlphaZero.uiScene.add(healthMesh);
    healthMesh.position.set(- 6. * widgetWidth, - window.innerHeight / 2. + 7 * widgetHeight / 16, 1.0);

    var armorTexture = textureLoader.load('textures/trak_light2.jpg');
    texture.magFilter = THREE.NearestFilter;
    texture.minFilter = THREE.LinearMipMapLinearFilter;
    var armorGeometry = new THREE.PlaneBufferGeometry(widgetWidth / 2, widgetHeight / 2);
    var armorMaterial = new THREE.MeshBasicMaterial({map: armorTexture,
          transparent: true,  opacity: .7, depthWrite: false});
    var armorMesh = new THREE.Mesh(armorGeometry, armorMaterial);
    AlphaZero.uiScene.add(armorMesh);
    armorMesh.position.set(- 1.5 * widgetWidth, - window.innerHeight / 2. + 7 * widgetHeight / 16, 1.0);

    var weaponTexture = textureLoader.load('textures/trak_weaponspawn_1.jpg');
    texture.magFilter = THREE.NearestFilter;
    texture.minFilter = THREE.LinearMipMapLinearFilter;
    var weaponGeometry = new THREE.PlaneBufferGeometry(widgetWidth / 2, widgetHeight / 2);
    var weaponMaterial = new THREE.MeshBasicMaterial({map: weaponTexture,
          transparent: true,  opacity: .7, depthWrite: false});
    var weaponMesh = new THREE.Mesh(weaponGeometry, weaponMaterial);
    AlphaZero.uiScene.add(weaponMesh);
    weaponMesh.position.set(3 * widgetWidth, - window.innerHeight / 2. + 7 * widgetHeight / 16, 1.0);

    // crosshair
    var crossHairTexture = textureLoader.load('textures/crosshair.png');
    crossHairTexture.magFilter = THREE.NearestFilter;
    crossHairTexture.minFilter = THREE.LinearMipMapLinearFilter;
    crossHairTexture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    crossHairTexture.repeat.x = 1 / 4;
    crossHairTexture.repeat.y = 1;
    var crossHairGeometry = new THREE.PlaneBufferGeometry(32, 32);
    var crossHairMaterial = new THREE.MeshBasicMaterial({map: crossHairTexture,
          transparent: true,  opacity: .7, depthWrite: false, color: 0x77dddd});
    var crossHairMesh = new THREE.Mesh(crossHairGeometry, crossHairMaterial);
    AlphaZero.uiScene.add(crossHairMesh);
    AlphaZero.crosshair = crossHairMesh;
  }

  function initGraphicsAndPhysics(name, isLocal) {
    Object.assign(AlphaZero, {
      "localPlayer": undefined,
      "players": [],
      "playersTHREE": [],
      "powerUps": [],
      "bullets": [],
      "misc": [],
      "mirrors": [],
      "water": [],
      "timeouts": [],
      "dynamicsWorld": undefined,
      "scene": undefined,
      "uiScene": undefined,
      "camera": undefined,
      "light": undefined,
      "pause": false,
      "mapTime": 120,
      "time": 120,
      "hud": undefined,
      "audioManager": undefined
    });

    // Setup THREE.js graphics

    // create scene and camera
    var scene = new THREE.Scene();
    AlphaZero.scene = scene;
    var uiScene = new THREE.Scene();
    AlphaZero.uiScene = uiScene;
    // create fog
    scene.fog = new THREE.Fog(0x000000, 12, 125);
    // setup camera
    var camera = new THREE.PerspectiveCamera(45, 
        window.innerWidth / window.innerHeight, 0.1, 100);
    camera.position.z = 8;
    AlphaZero.camera = camera;
    var uiCamera = new THREE.OrthographicCamera(window.innerWidth / -2., window.innerWidth / 2., window.innerHeight / 2., window.innerHeight / -2, 1, 500);
    uiCamera.position.set(0, 0, 300);
    AlphaZero.uiCamera = uiCamera;
    // create lights
    var ambient = new THREE.AmbientLight(0x080808);
    scene.add( ambient );
    var light = new THREE.SpotLight(0xffffff);
    light.position.set(0, 0, 12);
    light.target.position.set(0, 0, 0);
    light.castShadow = true;
    light.shadow.camera.near = 1;
    light.shadow.camera.far = 50;
    light.shadow.camera.fov = 45;
    light.shadow.mapSize.width = 4*512;
    light.shadow.mapSize.height = 4*512;
    scene.add(light);
    AlphaZero.light = light;

    // Setup Ammo.js physics
    var collisionConf = new Ammo.btDefaultCollisionConfiguration();
    var dispatcher = new Ammo.btCollisionDispatcher(collisionConf);
    var overlappingPairCache = new Ammo.btDbvtBroadphase();
    var solver = new Ammo.btSequentialImpulseConstraintSolver();

    var dynamicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher,
        overlappingPairCache, solver, collisionConf);
    dynamicsWorld.setGravity(new Ammo.btVector3(0, 0, -10));

    function collisionProcessedCallbackFunc(cp, colObj0, colObj1)
    {
      colObj0 = Ammo.wrapPointer(colObj0, Ammo.btRigidBody);
      colObj1 = Ammo.wrapPointer(colObj1, Ammo.btRigidBody);
      cp = Ammo.wrapPointer(cp, Ammo.btManifoldPoint);
      var player  = ((colObj0.type == "PL") && colObj0) ||
          ((colObj1.type == "PL") && colObj1) || undefined;
      var powerup = ((colObj0.type == "PU") && colObj0) ||
          ((colObj1.type == "PU") && colObj1) || undefined;
      var bullet  = ((colObj0.type == "BL") && colObj0) ||
          ((colObj1.type == "BL") && colObj1) || undefined;
      var lava  = ((colObj0.type == "LV") && colObj0) ||
          ((colObj1.type == "LV") && colObj1) || undefined;
      if (player && powerup) {
        if (powerup.AlphaZeroObject) {
          if (powerup.AlphaZeroObject.subType == "life") {
            player.AlphaZeroObject.life += powerup.AlphaZeroObject.value;
            AlphaZero.audioManager.playSound("powerupLife");
          }
          else if (powerup.AlphaZeroObject.subType == "armor") {
            player.AlphaZeroObject.armor += powerup.AlphaZeroObject.value;
            AlphaZero.audioManager.playSound("powerupArmour");
          }
          powerup.AlphaZeroObject.removeObject(true);
        }          
      }
      else if (player && bullet && !bullet.isGone) {
        var dmg = 20;
        if (player.AlphaZeroObject) {
          player.AlphaZeroObject.armor -= dmg;
          if (player.AlphaZeroObject.armor < 0) {
            player.AlphaZeroObject.life += player.AlphaZeroObject.armor;
            player.AlphaZeroObject.armor = 0;
            if (player.AlphaZeroObject.life <= 0) {
              player.AlphaZeroObject.removeObject(true);
            }
          }
          bullet.isGone = true;
          bullet.AlphaZeroObject.removeObject();
          AlphaZero.audioManager.playSound("pain");
        }
      }
      else if (player && lava) {
        var dmg = 1;
        if (player.AlphaZeroObject) {
          player.AlphaZeroObject.armor -= dmg;
          if (player.AlphaZeroObject.armor < 0) {
            player.AlphaZeroObject.life += player.AlphaZeroObject.armor;
            player.AlphaZeroObject.armor = 0;
            if (player.AlphaZeroObject.life <= 0) {
              player.AlphaZeroObject.removeObject(true);
            }
          }
          AlphaZero.audioManager.playSound("pain");
        }
      }
      else if (player && !bullet && !powerup) {
        if (player.AlphaZeroObject && player.AlphaZeroObject.isLocal) {
          player.AlphaZeroObject.canJump = true;
        }
      }
    }

    var processedCallbackPointer = Ammo.addFunction(
        collisionProcessedCallbackFunc);
    dynamicsWorld.setContactProcessedCallback(processedCallbackPointer);

    AlphaZero.dynamicsWorld = dynamicsWorld;

    createHUD();

    // load audio
    AlphaZero.audioManager = new AudioManager ({
        "jump": 'audio/effects/jump_jack_01.wav',
        "land": 'audio/effects/land.wav',
        "pain": 'audio/effects/pain_jack_01.wav',
        "walk": 'audio/effects/footstep_jack_01.wav',
        "death": 'audio/effects/death_jack_01.wav',
        "powerupArmour": 'audio/effects/get_armour.wav',
        "powerupLife": 'audio/effects/powerup_03.wav',
        "shoot": 'audio/effects/gun_rifle_01.wav',
        "stage": 'audio/music/tokyo-mask-new-gods-call.ogg',
      },
      function () {
        AlphaZero.audioManager.playSound("stage", true);
      });
    AlphaZero.audioManager.load();
  }

  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // create and load objects

  var textureLoader = new THREE.TextureLoader();

  ////////////////////////////////////////////////////////////////////////
  // Basic static box
  function createStaticBox() {
    var that = {};

    // THREE.js
    that.createGeometry = function(sx, sy, sz, x, y, z) {
      return new THREE.BoxGeometry(sx, sy, sz);
    };

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var texture = textureLoader.load('textures/stoneu.png');
      texture.wrapS = THREE.RepeatWrapping;
      texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.set(sx / 2.0, sy / 2.0);
      return new THREE.MeshLambertMaterial({ map: texture });
    };

    that.createView = function(sx, sy, sz, x, y, z, receiveShadow, castShadow) {
      // Visible mesh
      var mesh = new THREE.Mesh(
          that.createGeometry(sx, sy, sz, x, y, z),
          that.createMaterial(sx, sy, sz, x, y, z));
      mesh.position.set(x, y, z);
      AlphaZero.scene.add(mesh);
      that.mesh = mesh;

      // Transparent mesh for shadow display
      if (receiveShadow) {
        var shadowMesh = new THREE.Mesh(
            new THREE.PlaneGeometry(sx, sy),
            new THREE.ShadowMaterial());
        shadowMesh.receiveShadow = true;
        shadowMesh.position.set(x, y, z + sz / 2.0);
        AlphaZero.scene.add(shadowMesh);
      }

      mesh.castShadow = !!castShadow;

      return mesh;
    };

    // Ammo / Bullet
    that.createPhysicsBody = function(sx, sy, sz, x, y, z, type, info, mass) {
      mass = mass || 0;
      // Ammo shape
      var boxShape = new Ammo.btBoxShape(new Ammo.btVector3(sx / 2.0,
          sy / 2.0, sz / 2.0));
      var boxTransform = new Ammo.btTransform();
      boxTransform.setIdentity();
      boxTransform.setOrigin(new Ammo.btVector3(x, y, z));
      // Ammo kinematics
      var localInertia = new Ammo.btVector3(0, 0, 0);
      var myMotionState = new Ammo.btDefaultMotionState(boxTransform);
      var rbInfo = new Ammo.btRigidBodyConstructionInfo(mass, myMotionState,
          boxShape, localInertia);
      boxShape.calculateLocalInertia(mass, localInertia);

      Object.assign(rbInfo, info);
      // Ammo body
      var body = new Ammo.btRigidBody(rbInfo);
      body.type = type;
      // Add to world
      AlphaZero.dynamicsWorld.addRigidBody(body);
      return body;
    };

    that.syncMeshWithBody = function() {
      // update mesh object
      var body = that.body;
      body.getMotionState().getWorldTransform(transform);
      var origin = transform.getOrigin();
      var mesh = that.mesh;
      mesh.position.x = origin.x();
      mesh.position.y = origin.y();
      mesh.position.z = origin.z() + 1/16;

      var rotation = transform.getRotation();
      rotation.setEulerZYX(mesh.rotation.z,
                           mesh.rotation.y,
                           mesh.rotation.x);

      return that;
    }

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.createView(sx, sy, sz, x, y, z, true, true);
      that.createPhysicsBody(sx, sy, sz, x, y, z, "GN", {});
      return that;
    }

    that.rsx = 0, that.rsy = 0, that.rsz = 0;
    that.rx = 0, that.ry = 0, that.rz = 0;
    that.removeObject = function() {
      AlphaZero.scene.remove(that.mesh);
      AlphaZero.dynamicsWorld.removeRigidBody(that.body);
      that.body.AlphaZeroObject = null;
      return that;
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Platform/Pillar
  function createPlatform(sx, sy, sz, x, y, z) {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var texture = textureLoader.load('textures/ground_texture.png');
      texture.wrapS = THREE.RepeatWrapping;
      texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.set(sx / 2.0, sy / 2.0);

      return new THREE.MeshPhongMaterial({ map: texture });
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Actual Battleground
  function createBattleground(sx, sy, sz, x, y, z) {
    var that = createStaticBox();

    that.createGeometry = function(sx, sy, sz, x, y, z) {
      var geometry = new THREE.BoxBufferGeometry(sx, sy, sz);
      geometry.groups[0].materialIndex = 1;
      geometry.groups[1].materialIndex = 1;
      geometry.groups[2].materialIndex = 2;
      geometry.groups[3].materialIndex = 2;
      geometry.groups[4].materialIndex = 0;
      geometry.groups[5].materialIndex = 0;

      return geometry;
    };

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var normalMapSX = textureLoader.load('textures/ground04_normal.png');
      var mapSX = textureLoader.load('textures/ground04_diffused.png');
      var specularMapSX = textureLoader.load('textures/ground04_specular.png');

      mapSX.wrapS = THREE.RepeatWrapping;
      mapSX.wrapT = THREE.RepeatWrapping;
      mapSX.repeat.set(sz / 2.0, sy / 2.0);
      normalMapSX.wrapS = THREE.RepeatWrapping;
      normalMapSX.wrapT = THREE.RepeatWrapping;
      normalMapSX.repeat.set(sz / 2.0, sy / 2.0);
      specularMapSX.wrapS = THREE.RepeatWrapping;
      specularMapSX.wrapT = THREE.RepeatWrapping;
      specularMapSX.repeat.set(sz / 2.0, sy / 2.0);

      var normalMapSY = textureLoader.load('textures/ground04_normal.png');
      var mapSY = textureLoader.load('textures/ground04_diffused.png');
      var specularMapSY = textureLoader.load('textures/ground04_specular.png');

      mapSY.wrapS = THREE.RepeatWrapping;
      mapSY.wrapT = THREE.RepeatWrapping;
      mapSY.repeat.set(sx / 2.0, sz / 2.0);
      normalMapSY.wrapS = THREE.RepeatWrapping;
      normalMapSY.wrapT = THREE.RepeatWrapping;
      normalMapSY.repeat.set(sx / 2.0, sz / 2.0);
      specularMapSY.wrapS = THREE.RepeatWrapping;
      specularMapSY.wrapT = THREE.RepeatWrapping;
      specularMapSY.repeat.set(sx / 2.0, sz / 2.0);

      var normalMap = textureLoader.load('textures/ground04_normal.png');
      var map = textureLoader.load('textures/ground04_diffused.png');
      var specularMap = textureLoader.load('textures/ground04_specular.png');

      map.wrapS = THREE.RepeatWrapping;
      map.wrapT = THREE.RepeatWrapping;
      map.repeat.set(sx / 2.0, sy / 2.0);
      normalMap.wrapS = THREE.RepeatWrapping;
      normalMap.wrapT = THREE.RepeatWrapping;
      normalMap.repeat.set(sx / 2.0, sy / 2.0);
      specularMap.wrapS = THREE.RepeatWrapping;
      specularMap.wrapT = THREE.RepeatWrapping;
      specularMap.repeat.set(sx / 2.0, sy / 2.0);

      var topMaterial = new THREE.MeshPhongMaterial({
        map: map,
        normalMap: normalMap,
        specularMap: specularMap
      });
      var sideMaterialY = new THREE.MeshPhongMaterial({
        map: mapSY,
        normalMap: normalMapSY,
        specularMap: specularMapSY
      });
      var sideMaterialX = new THREE.MeshPhongMaterial({
        map: mapSX,
        normalMap: normalMapSX,
        specularMap: specularMapSX
      });

      return [topMaterial, sideMaterialX, sideMaterialY];
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Reflective and Transparent Box
  function createGlassBox(camera, resolution, mirrorSign, xoffset, yoffset) {
    var that = createStaticBox();
    var material = undefined;
    var mirrorOffEnvMap = textureLoader.load('textures/glass_alpha_map.png');

    var near = camera.near;
    var far = camera.far;
    var fov = 90, aspect = 1;
    var mirrorCamera = new THREE.PerspectiveCamera( fov, aspect, near, far );
    mirrorCamera.up.set( 0, - 1, 0 );
    mirrorCamera.lookAt( new THREE.Vector3( 0, 0, - 1 ) );
    mirrorCamera.position.set(0, 0, 8);

    var options = { format: THREE.RGBFormat, magFilter: THREE.LinearFilter, minFilter: THREE.LinearFilter };

    var renderTarget = new THREE.WebGLRenderTargetCube( resolution, resolution, options );

    that.update = function (renderer, scene) {
      mirrorCamera.position.z = AlphaZero.camera.position.z
      var y = AlphaZero.camera.position.y;
      var x = AlphaZero.camera.position.x;
      mirrorCamera.position.x = mirrorSign * (- 2 * that.mesh.position.x + x + xoffset);
      mirrorCamera.position.y = mirrorSign * (2 * that.mesh.position.y - y  + yoffset);
      mirrorCamera.updateMatrixWorld();
      var generateMipmaps = renderTarget.texture.generateMipmaps;
      renderTarget.texture.generateMipmaps = generateMipmaps;
      renderTarget.activeCubeFace = 5;
      renderer.render( scene, mirrorCamera, renderTarget );
      renderer.setRenderTarget( null );
    };

    that.clear = function (renderer, color, depth, stencil) {
      renderTarget.activeCubeFace = 5;
      renderer.setRenderTarget( renderTarget );
      renderer.clear( color, depth, stencil );
      renderer.setRenderTarget( null );
    };

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      material = new THREE.MeshPhongMaterial({
          color: 0xffffff,
          envMap: renderTarget.texture,
          transparent: true,
          alphaMap: textureLoader.load('textures/glass_alpha_map.png'),
          shininess: 100
        });

      return material;
    };

    that.toggleMirrorMaterial = function(state) {
      if (!state) {
        material.envMap = mirrorOffEnvMap;
      }
      else {
        material.envMap = renderTarget.texture;
      }
    }

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.createView(sx, sy, sz, x, y, z, true, false);
      that.createPhysicsBody(sx, sy, sz, x, y, z, "GN", {m_restitution:  3.0});
      AlphaZero.mirrors.push(that);
      return that;
    }

    return that;
  };

  ////////////////////////////////////////////////////////////////////////
  // Lava Box
  function createLavaBox() {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      uniforms = {
        fogDensity: {value: 0.05},
        fogColor: {value: new THREE.Vector3( 0, 0, 0 )},
        time: {value: 1.0},
        uvScale: {value: new THREE.Vector2( 1.0, 3.0 )},
        texture1: {value: textureLoader.load('textures/cloud.png')},
        texture2: {value: textureLoader.load('textures/lavatile.jpg')}
      };

      uniforms.texture1.value.wrapS = uniforms.texture1.value.wrapT =
          THREE.RepeatWrapping;
      uniforms.texture2.value.wrapS = uniforms.texture2.value.wrapT =
          THREE.RepeatWrapping;

      return new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: document.getElementById(
            'vertexShader').textContent,
        fragmentShader: document.getElementById(
            'fragmentShader').textContent
      });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.createView(sx, sy, sz, x, y, z, false, false);
      that.createPhysicsBody(sx, sy, sz, x, y, z, "LV", {});
      return that;
    }

    return that;
  };

  ////////////////////////////////////////////////////////////////////////
  // Water Box

  function createWaterBox2() {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      return new THREE.MeshPhongMaterial({ map: textureLoader.load('textures/ny.jpg') });
    };

    return that;
  };

  function createWaterBox() {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      return textureLoader.load('textures/waternormals.jpg');
    };

    that.createView = function(sx, sy, sz, x, y, z, receiveShadow, castShadow) {
      // Visible mesh
      var waterGeometry = new THREE.PlaneBufferGeometry( sx, sy );
      var flowMap = textureLoader.load( 'textures/Water_1_M_Flow.jpg' );

      AlphaZero.water = new THREE.Water( waterGeometry, {
        scale: 2,
        textureWidth: 1024,
        textureHeight: 1024,
        flowMap: flowMap,
        exposure : .35,
      } );
      that.mesh = AlphaZero.water;

      AlphaZero.water.position.set(x, y, z);
      AlphaZero.scene.add(AlphaZero.water);
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.createView(sx, sy, sz, x, y, z, false, false);
      // that.createPhysicsBody(sx, sy, sz, x, y, z, "LV", {});
      return that;
    }

    return that;
  };

  function createCloudBox() {
    var that = createStaticBox();

    that.createGeometry = function(sx, sy, sz, x, y, z) {
      return new THREE.PlaneBufferGeometry(sx, sy);
    }

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var hudUniforms = {
        uvScale: {value: new THREE.Vector2(10.0, 10.0)},
        iResolution: {value: new THREE.Vector2(1.0, 1.0)},
        iTime: {value: 1.0},
        iChannel0: {value: textureLoader.load('textures/rgb_noise.png')},
      };
      hudUniforms.iChannel0.value.wrapS = hudUniforms.iChannel0.value.wrapT = THREE.RepeatWrapping;
      return new THREE.RawShaderMaterial({
        uniforms: hudUniforms,    
        vertexShader: document.getElementById(
                'cloudVertexShader').textContent,
        fragmentShader: document.getElementById(
                'cloudFragmentShader').textContent,
        // transparent: true
      });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.createView(sx, sy, sz, x, y, z, false, false);
      that.createPhysicsBody(sx, sy, sz, x, y, z, "LV", {});
      return that;
    }

    return that;
  };

  //////////////////////////////////////////////////////////////////////////////
  // Player object
  function createPlayer(name, isLocal) {
    var that = createStaticBox();
    var texture = undefined;

    // THREE.js
    that.createGeometry = function(sx, sy, sz, x, y, z) {
      return new THREE.CylinderGeometry(1/6, 1/4, 1/3, 8);
    };

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      return new THREE.MeshBasicMaterial({color: 0xffff00,
          transparent: true,  opacity: 0.0, depthWrite: false});
    };

    that.createOffsetGeometry = function(sx, sy, sz, x, y, z) {
      return new THREE.PlaneGeometry(1, 1);
    };

    that.createOffsetMaterial = function(sx, sy, sz, x, y, z) {
      texture = textureLoader.load('textures/marine01_sheet.png');
      texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.x = 1 / 8;
      texture.repeat.y = 1 / 8;
      return new THREE.MeshLambertMaterial({ map: texture,
          transparent: true, depthWrite: false });
    };

    that.createView = function(sx, sy, sz, x, y, z, receiveShadow, castShadow) {
      // Transparent mesh for shadow display
      var mesh = new THREE.Mesh(
          that.createGeometry(sx, sy, sz, x, y, z),
          that.createMaterial(sx, sy, sz, x, y, z));
      mesh.position.set(x, y, z);
      mesh.castShadow = true;
      mesh.rotation.x = Math.PI / 2;
      AlphaZero.scene.add(mesh);
      mesh.castShadow = !!castShadow;
      AlphaZero.scene.add(mesh);

      // Visible offset mesh
      var offsetMesh = new THREE.Mesh(
          that.createOffsetGeometry(sx, sy, sz, x, y, z),
          that.createOffsetMaterial(sx, sy, sz, x, y, z));
      offsetMesh.position.set(x, y, z);
      AlphaZero.scene.add(offsetMesh);

      return [mesh, offsetMesh];
    };

    // animation handling
    // 'animationName': [startFrame, endFrame)
    var animations = {
      'standing': [0, 0],
      'walking': [0, 20],
      'shooting': [25, 49]
    };
    var state = 'standing';
    var frameNum = animations[state][0];
    var up = true;
    var playOnce = false;
    that.setNextFrame = function(st, once) {
      if (st != state && !playOnce) {
        state = st;
        frameNum = animations[state][0];
        up = true;
      }
      playOnce = playOnce || once;
      if (frameNum == animations[state][1]) {
        up = false;
        if (playOnce) {
          state = 'standing';
          frameNum = animations[state][0];
          up = true;
          playOnce = false;
        }
      } 
      else if (frameNum == animations[state][0]) {
        up = true;
      }
      if (state != 'standing') {
        frameNum += up ? 4 : -4;
      }
      texture.offset.x = (frameNum % 8) / 8;
      texture.offset.y = (- 1 - Math.floor(frameNum / 8)) / 8;

      return that;
    }

    var superSyncMeshWithBody = that.syncMeshWithBody;
    that.syncMeshWithBody = function() {
      superSyncMeshWithBody();
      var mesh = that.mesh;

      var position = that.shadowMesh.position;
      position.x = mesh.position.x;
      position.y = mesh.position.y;
      position.z = mesh.position.z;

      var v = new THREE.Vector3(0, 1/8, 0);
      var quaternion = new THREE.Quaternion();
      var rotation = transform.getRotation();
      quaternion.x = rotation.x();
      quaternion.y = rotation.y();
      quaternion.z = rotation.z();
      quaternion.w = rotation.w();
      v.applyQuaternion(quaternion);
      mesh.position.add(v);

      // live camera
      if (that.isLocal && !querySettings['stable-camera']) {
        var x = mesh.position.x;
        var y = mesh.position.y;
        AlphaZero.camera.position.x = x;
        AlphaZero.camera.position.y = y;
      }
      // stable camera
      else if (that.isLocal && querySettings['stable-camera']) {
        var x = position.x;
        var y = position.y;
        AlphaZero.camera.position.x = x;
        AlphaZero.camera.position.y = y;
      }
      return that;
    }

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.rsx = sx, that.rsy = sy, that.rsz = sz;
      that.rx = x, that.ry = y, that.rz = z;
      var meshes = that.createView(sx, sy, sz, x, y, z, true, true);
      that.type = "PL";
      that.name = name;
      that.isLocal = isLocal;
      that.life = 100;
      that.armor = 100;
      that.canJump = false;
      that.shadowMesh = meshes[0];
      that.mesh = meshes[1];
      that.spriteMesh = meshes[1];
      that.body = that.createPhysicsBody(sx, sy, sz, x, y, z, "PL", {}, 70);
      that.body.setActivationState(4); // unclear if CF_NO_CONTACT_RESPONSE
                                       // OR CF_CHARACTER_OBJECT
      that.body.activate();      
      that.setNextFrame = that.setNextFrame;

      that.body.AlphaZeroObject = that;
      AlphaZero.players.push(that);
      AlphaZero.playersTHREE.push(that.shadowMesh);
      if(that.isLocal) {
        AlphaZero.localPlayer = that;
      }

      return that;
    }

    that.removeObject = function(respawn) {
      var index = AlphaZero.players.indexOf(that);
      if (index >= 0) {
        AlphaZero.scene.remove(that.mesh);
        AlphaZero.scene.remove(that.shadowMesh);
        AlphaZero.dynamicsWorld.removeRigidBody(that.body);
        that.body.AlphaZeroObject = null;
        AlphaZero.players.splice(index, 1);
        if (respawn) {
          // TODO: this needs amending to prevent growing without limits
          AlphaZero.timeouts.push(setTimeout(that.initObject, 2000,
            that.rsx, that.rsy, that.rsz,
            that.rx, that.ry, that.rz));
        }
        AlphaZero.audioManager.playSound("death");
      }
      return that;
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Armor power up
  function createArmorPowerUp(value) {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var texture = textureLoader.load('textures/trak_light2.jpg');
      return new THREE.MeshLambertMaterial({ map: texture });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.rsx = sx, that.rsy = sy, that.rsz = sz;
      that.rx = x, that.ry = y, that.rz = z;
      that.type = "PU";
      that.subType = "armor";
      that.value = value;
      that.mesh = that.createView(sx, sy, sz, x, y, z, false, true);
      that.body = that.createPhysicsBody(sx, sy, sz, x, y, z, "PU", {}, 1);

      that.body.AlphaZeroObject = that;
      AlphaZero.powerUps.push(that);

      return that;
    }

    that.removeObject = function(respawn) {
      var index = AlphaZero.powerUps.indexOf(that);
      if (index >= 0) {
        AlphaZero.scene.remove(that.mesh);
        AlphaZero.dynamicsWorld.removeRigidBody(that.body);
        that.body.AlphaZeroObject = null;
        AlphaZero.powerUps.splice(index, 1);
        if (respawn) {
          AlphaZero.timeouts.push(setTimeout(that.initObject, 2000,
              that.rsx, that.rsy, that.rsz,
              that.rx, that.ry, that.rz));
        }
      }
      return that;
    }

    var superSyncMeshWithBody = that.syncMeshWithBody;
    that.syncMeshWithBody = function() {
      superSyncMeshWithBody();
      that.mesh.position.z += 1/4;
      return that;
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Life power up
  function createLifePowerUp(value) {
    var that = createArmorPowerUp(0);

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var lifeTexture = textureLoader.load('textures/trak_light2a.jpg');
      return new THREE.MeshLambertMaterial({ map: lifeTexture });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.rsx = sx, that.rsy = sy, that.rsz = sz;
      that.rx = x, that.ry = y, that.rz = z;
      that.type = "PU";
      that.subType = "life";
      that.value = value;
      that.mesh = that.createView(sx, sy, sz, x, y, z, false, true);
      that.body = that.createPhysicsBody(sx, sy, sz, x, y, z, "PU", {}, 1);

      that.body.AlphaZeroObject = that;
      AlphaZero.powerUps.push(that);

      return that;
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Basic movable box
  function createBox() {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      var normalMap = textureLoader.load('textures/ab_crate_a_nm.png');
      var map = textureLoader.load('textures/ab_crate_a.png');
      var specularMap = textureLoader.load('textures/ab_crate_a_sm.png');
      return new THREE.MeshPhongMaterial({
        map: map,
        normalMap: normalMap,
        specularMap: specularMap
      });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.type = "BO";
      that.mesh = that.createView(sx, sy, sz, x, y, z, false, true);
      that.body = that.createPhysicsBody(sx, sy, sz, x, y, z, "BO", {}, 70);

      that.body.AlphaZeroObject = that;
      AlphaZero.misc.push(that);
      return that;
    }

    return that;
  }

  ////////////////////////////////////////////////////////////////////////
  // Simple bullet
  function createBullet(velocity) {
    var that = createStaticBox();

    that.createMaterial = function(sx, sy, sz, x, y, z) {
      return new THREE.MeshBasicMaterial({ color: 0xff0000,
      wireframe: true });
    };

    that.initObject = function(sx, sy, sz, x, y, z) {
      that.type = "BL";
      that.mesh = that.createView(sx, sy, sz, x, y, z, false, true);
      that.body = that.createPhysicsBody(sx, sy, sz, x, y, z, "BL", {}, 1);

      that.body.setLinearVelocity(velocity);
      that.body.AlphaZeroObject = that;
      AlphaZero.bullets.push(that);

      AlphaZero.timeouts.push(setTimeout(that.removeObject, 2000, that.body));
      return that;
    }

    that.removeObject = function() {
      var index = AlphaZero.bullets.indexOf(that);
      if (index >= 0) {
        AlphaZero.scene.remove(that.mesh);
        AlphaZero.dynamicsWorld.removeRigidBody(that.body);
        AlphaZero.bullets.splice(index, 1);
        that.body.AlphaZeroObject = null;
      }
      return that;
    }

    return that;
  }

  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // Bullet Physics

  var transform = new Ammo.btTransform(); // taking this out of
                                          // readBulletObject reduces
                                          // the leaking
  function simulate(dt) {
    dt = dt || 1 / 60;
    // Cap simulation time
    dt = dt > 0.3 ? 1 / 60 : dt;
    
    AlphaZero.dynamicsWorld.stepSimulation(dt, 5);

    // Read bullet data into JS objects
    AlphaZero.players.forEach(function(object) {
      object.syncMeshWithBody();
    });
    AlphaZero.powerUps.forEach(function(object) {
      object.syncMeshWithBody();
    });
    AlphaZero.bullets.forEach(function(object) {
      object.syncMeshWithBody();
    });
    AlphaZero.misc.forEach(function(object) {
      object.syncMeshWithBody();
    });
  }

  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // Controls

  // Keybord actions
  var mouse = {};
  var actions = {};
  var keysActions = {
    "KeyW":'up',
    "KeyS":'down',
    "KeyA":'left',
    "KeyD":'right',
    "Space":'jump',
    "KeyP": "pause"
  };

  // Events we are interested in
  window.addEventListener('resize', onWindowResize, false);
  window.addEventListener('keydown', onKeyDown);
  window.addEventListener('keyup', onKeyUp);
  window.addEventListener('mousemove', onDocumentMouseMove, false);
  window.addEventListener('wheel', onDocumentMouseWheel, false);
  window.addEventListener('click', onDocumentMouseClick, false);
  
  function onWindowResize() {
    var width = window.innerWidth;
    var height = window.innerHeight;
    AlphaZero.camera.aspect = width / height;
    AlphaZero.camera.updateProjectionMatrix();
    AlphaZero.uiCamera.left = - width / 2;
    AlphaZero.uiCamera.right = width / 2;
    AlphaZero.uiCamera.top = height / 2;
    AlphaZero.uiCamera.bottom = - height / 2;
    // AlphaZero.uiCamera.aspect = width / height;
    AlphaZero.uiCamera.updateProjectionMatrix();
    AlphaZero.renderer.setSize(width, height);
  }

  function onKeyUp(e) {
    if(keysActions[e.code]) {
      actions[keysActions[e.code]] = false;
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  function onKeyDown(e) {
    if(keysActions[e.code]) {
      actions[keysActions[e.code]] = true;
      if(keysActions[e.code] == "pause") {
        AlphaZero.pause = !AlphaZero.pause;
        if (AlphaZero.pause) {
          animate();
        }
      }
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  function onDocumentMouseClick(event) {
    event.preventDefault();
    event.stopPropagation();

    var height = window.innerHeight;
    var width = window.innerWidth;
    mouse.x = (event.clientX / width) * 2 - 1;
    mouse.y = - (event.clientY / height) * 2 + 1;
    actions['shootPrimary'] = true;
    return false;
  }

  function onDocumentMouseMove(event) {
    event.preventDefault();
    event.stopPropagation();

    var height = window.innerHeight;
    var width = window.innerWidth;
    mouse.x = (event.clientX / width) * 2 - 1;
    mouse.y = - (event.clientY / height) * 2 + 1;
    AlphaZero.crosshair.position.set(event.clientX - width / 2, - event.clientY + height / 2, 1.);

    var raycaster = new THREE.Raycaster();
    var m = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    raycaster.setFromCamera(m, AlphaZero.camera);
    var intersects = raycaster.intersectObjects(AlphaZero.playersTHREE);
    if (intersects.length) {
      AlphaZero.crosshair.material.map.offset.x = 3 / 4;
    } else {
      AlphaZero.crosshair.material.map.offset.x = 0;
    }

    return false;
  }

  function onDocumentMouseWheel(event) {
    event.preventDefault();
    event.stopPropagation();

    AlphaZero.camera.position.z += (event.deltaY > 0) ? 0.1 : - 0.1;
    return false;
  }

  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // Game loop and rendering stats

  // Add Stats
  var stats = undefined;
  if (querySettings['show-fps']) {
    stats = new Stats();
    document.body.appendChild(stats.domElement);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    stats.domElement.style.left = '0px';
  }

  var lastRender = undefined;
  var lastTick = undefined;

  var animate = function() {
    // reschedule
    if (!AlphaZero.pause) {
      requestAnimationFrame(animate);
    }
    else {
      return;
    }

    // delta time
    lastRender = lastRender ? lastRender : Date.now();
    lastTick = lastTick ? lastTick : Date.now();
    var atm = Date.now();
    var dt = atm - lastRender;
    var dt2 = (atm - lastTick) / 1000;
    lastTick = atm;
    AlphaZero.time -= dt2;

    var animationToPlay = 'standing';
    var playOnce = false;

    // update from controls
    var body = AlphaZero.localPlayer.body;
    var v = new Ammo.btVector3(0, 0, 0);
    if (actions.up) {
      v.setY(2);
      animationToPlay = 'walking';
      playOnce = false;
    }
    if (actions.down) {
      v.setY(-2);
      animationToPlay = 'walking';
      playOnce = false;
    }
    if (actions.left) {
      v.setX(-2);
      animationToPlay = 'walking';
      playOnce = false;
    }
    if (actions.right) {
      v.setX(2);
      animationToPlay = 'walking';
      playOnce = false;
    }
    // speed cap
    if (v.length() > 2.0) {
      v.normalize();
      v.op_mul(2);
    }
    if (v.length() > 0.1) {
      AlphaZero.audioManager.playSound("walk");
    }
    if (AlphaZero.localPlayer.canJump && actions.jump) {
      v.op_mul(0);
      v.setZ(4);
      AlphaZero.localPlayer.canJump = false;
      AlphaZero.audioManager.playSound("jump");
    } else {
      v.setZ(body.getLinearVelocity().z());
    }
    body.setLinearVelocity(v);

    // point gun at mouse pointer and prevent player rotation in Ammo
    var mesh = AlphaZero.localPlayer.mesh;
    var ppos = mesh.position.clone().project(AlphaZero.camera);
    var angle = Math.atan2(mouse.y - ppos.y, mouse.x - ppos.x);
    if (angle) {
      angle -= Math.PI / 2;
      var quat = new Ammo.btQuaternion();
      quat.setEulerZYX(angle, 0, 0);
      body.getWorldTransform().setRotation(quat);

       body.getMotionState().getWorldTransform(transform);
       var rotation = transform.getRotation();
       var quaternion = new THREE.Quaternion();
       quaternion.x = rotation.x();
       quaternion.y = rotation.y();
       quaternion.z = rotation.z();
       quaternion.w = rotation.w();
       mesh.rotation.setFromQuaternion(quaternion);
    }

    // shoot
    if (actions.shootPrimary) {
      var body = AlphaZero.localPlayer.body;
      body.getMotionState().getWorldTransform(transform);

      var rotation = transform.getRotation();
      var quaternion = new THREE.Quaternion();
      quaternion.x = rotation.x();
      quaternion.y = rotation.y();
      quaternion.z = rotation.z();
      quaternion.w = rotation.w();

      var position = AlphaZero.localPlayer.shadowMesh.position.clone();
      position.x = mesh.position.x;
      position.y = mesh.position.y;
      position.z = mesh.position.z;

      var v = new THREE.Vector3(0, 1/3, 0);
      v.applyQuaternion(quaternion);
      position.add(v);

      var raycaster = new THREE.Raycaster();
      var m = new THREE.Vector3(mouse.x, mouse.y, 0.5);
      raycaster.setFromCamera(m, AlphaZero.camera);
      var intersects = raycaster.intersectObjects(
          AlphaZero.scene.children);
      if (intersects.length) {
        var intPoint = intersects[0].point
        var velocity = new Ammo.btVector3(intPoint.x - position.x,
            intPoint.y - position.y, intPoint.z - position.z);
        velocity.z = 0;
        velocity.normalize();
        velocity.op_mul(10);
        createBullet(velocity).initObject(1/32, 1/32, 1/32,
            position.x, position.y, position.z);
      }
      actions.shootPrimary = false;
      animationToPlay = 'shooting';
      playOnce = true;
      AlphaZero.audioManager.playSound("shoot", false, true);
    }

    // play animation at about 30 fps / sec
    // requestAnimationFrame shoots for 60
    if (dt > 1000 / 30 || playOnce) {
      // play animations
      // AlphaZero.players.forEach(function(object) {
      //   object.setNextFrame(animationToPlay, playOnce);
      // });
      AlphaZero.localPlayer.setNextFrame(animationToPlay, playOnce);
      lastRender = atm;
    }

    // powerup animation handling
    AlphaZero.powerUps.forEach(function(object) {
      if (object.subType == "life") {
        object.mesh.rotation.z += 0.1;
      }
      else if (object.subType == "armor") {
        object.mesh.rotation.x += 0.1;
        object.mesh.rotation.y += 0.1;
      }
    });

    // physics
    simulate(dt2);

    // TODO
    // AlphaZero.water.material.uniforms.time.value += 1.0 / 60.0;

    // hud update
    var dispTime = Math.ceil(AlphaZero.time);
    var ns = 4;
    if (dispTime > 0) {
      ns = 4 - Math.log10(dispTime + 1);
    }
    var text = ("TIME"+" ".repeat(ns) + dispTime);
    AlphaZero.hud.material.uniforms.index.value = textToTextureIndex(text);
    ns = 3;
    if (AlphaZero.localPlayer.life > 0) {
      ns = 3 - Math.log10(AlphaZero.localPlayer.life + 1);
    }
    var healthText = ("    "+" ".repeat(ns) + AlphaZero.localPlayer.life);
    AlphaZero.hudHealth.material.uniforms.index.value = textToTextureIndex(healthText);
    ns = 3;
    if (AlphaZero.localPlayer.armor > 0) {
      ns = 3 - Math.log10(AlphaZero.localPlayer.armor + 1);
    }
    var armorText = ("    "+" ".repeat(ns) + AlphaZero.localPlayer.armor);
    AlphaZero.hudArmor.material.uniforms.index.value = textToTextureIndex(armorText);    

    // render on canvas
    // TODO: get rid of global reference
    uniforms.time.value += dt2;
    // AlphaZero.renderer.clear();
    AlphaZero.mirrors.forEach(function(object) {
      // TODO: get rid of this
      object.toggleMirrorMaterial(false);
      object.clear(AlphaZero.renderer, 0x000000, true, true);
      object.update(AlphaZero.renderer, AlphaZero.scene);
      // TODO: get rid of this
      object.toggleMirrorMaterial(true);
    });
    AlphaZero.renderer.render(AlphaZero.scene, AlphaZero.camera);
    AlphaZero.renderer.clearDepth();
    AlphaZero.renderer.render(AlphaZero.uiScene, AlphaZero.uiCamera);
    // update fps indicator
    if (stats) {
      stats.update();
    }
  };

  //////////////////////////////////////////////////////////////////////////////  //////////////////////////////////////////////////////////////////
  // get things moving

  function createDeathmathMap() {
    createPlatform().initObject(12, 12, 36, 0, 0, -20);
    createBattleground().initObject(12, 12, 2, 0, 0, -1);

    // starting position
    createStaticBox().initObject(2, 2, 1, -5, -5, 0.5);
    createStaticBox().initObject(2, 2, 1, 5, 5, 0.5);
    createStaticBox().initObject(2, 2, 1, -5, 5, 0.5);
    createStaticBox().initObject(2, 2, 1, 5, -5, 0.5);
    // trophy stand
    createStaticBox().initObject(2, 2, 1, 0, 0, 0.5);
   
    // surrounding walls
    createGlassBox(AlphaZero.camera, 512, -1., -0.0625, 0.).initObject(0.125, 13, 6, -6.0625, 0, 0.5);
    createGlassBox(AlphaZero.camera, 512, -1., 0.0625, 0. ).initObject(0.125, 13, 6, 6.0625, 0, 0.5);
    createGlassBox(AlphaZero.camera, 512, 1., 0., -0.0625).initObject(13, 0.125, 6, 0, 6.0625, 0.5);
    createGlassBox(AlphaZero.camera, 512, 1., 0., 0.0625).initObject(13, 0.125, 6, 0, -6.0625, 0.5);
    // lava
    createLavaBox().initObject(1, 8, 0.02, -5.5, 0, 0.02);
    // water
    createWaterBox2().initObject(4, 4, 1., 0, 0, .5);
    createWaterBox().initObject(4, 4, 1., 0, 0, 1.);
    // clouds
    // createCloudBox().initObject(4, 4, 1., 0, 0, 2.);
    // other moving objects
    createBox().initObject(1/2, 1/2, 1/2, 3, 3, 3);
    // other battlegrounds
    createPlatform().initObject(12, 12, 36, 24, 0, -20);
    createBattleground().initObject(12, 12, 2, 24, 0, -1);
    createPlatform().initObject(12, 12, 36, -24, 0, -20);
    createBattleground().initObject(12, 12, 2, -24, 0, -1);
    createPlatform().initObject(12, 12, 36, 0, 24, -20);
    createBattleground().initObject(12, 12, 2, 0, 24, -1);
    createPlatform().initObject(12, 12, 36, 0, -24, -20);
    createBattleground().initObject(12, 12, 2, 0, -24, -1);
    // power ups
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, 5, 5, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, 5, 0, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, 5, -5, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, 0, -5, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, -5, -5, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, -5, 0, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, -5, 5, 5);
    createLifePowerUp(20).initObject(1/4, 1/4, 1/4, 0, 5, 5);
    for (var i = 0; i < 8; i++) {
      createArmorPowerUp(20).initObject(1/4, 1/4, 1/4, 0, (i - 4)*1.4, 5);
    }
    // add players
    createPlayer("Player One", true).initObject(1/6, 1/4, 1/3, -3, 0, 5);
    createPlayer("Player Two", false).initObject(1/6, 1/4, 1/3, -4.5, 0, 5);
  }

  function reset() {
    AlphaZero.timeouts.forEach(function(id) {
      clearTimeout(id);
    });
    initGraphicsAndPhysics();
    createDeathmathMap();
    AlphaZero.timeouts.push(setTimeout(reset, AlphaZero.mapTime * 1000));
  }

  initRenderer();
  reset();
  // go
  animate();
});
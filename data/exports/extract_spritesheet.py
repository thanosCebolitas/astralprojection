# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 03:28:48 2018

@author: nala
"""

import imageio
import numpy as np
#im = imageio.imread('marine01.png')
#imageio.imwrite('marine01_move.png', im[:,:24*256,:])
#imageio.imwrite('marine01_shoot.png', im[:,48*256:72*256,:])
#im1 = imageio.imread('marine01_move.png')
#im2 = imageio.imread('marine01_shoot.png')


m1 = np.zeros((256, 8*256, 4))
for b in range(8):
    m1[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(b)+'.png')
m2 = np.zeros((256, 8*256, 4))
for b in range(8):
    m2[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(8+b)+'.png')
m3 = np.zeros((256, 8*256, 4))
for b in range(8):
    m3[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(16+b)+'.png')

s1 = np.zeros((256, 8*256, 4))
for b in range(8):
    s1[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(48+b)+'.png')
s2 = np.zeros((256, 8*256, 4))
for b in range(8):
    s2[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(56+b)+'.png')
s3 = np.zeros((256, 8*256, 4))
for b in range(8):
    s3[:,256*b:256*b+256,:] = imageio.imread('marine01.'+'{:04}'.format(64+b)+'.png')

wim = np.vstack((m1,m2,m3,s1,s2,s3,s2,s3))
imageio.imwrite('marine01_sheet.png', wim)